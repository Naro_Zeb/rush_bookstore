package com.bookstore.controller;
import static org.hamcrest.CoreMatchers.containsString;
import static
org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static
org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static
org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
@RunWith(SpringRunner.class)
@WebMvcTest(CheckoutController.class)

public class ShopingCartControllerTest {
	@Autowired
	private MockMvc mockMvc;

	@Test
	public void testCartPage() throws Exception {
		mockMvc.perform(get("/cart"))
		.andExpect(status().isOk())
		.andExpect(view().name("shoppingCart"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}
	@Test
	public void testaddItemPage() throws Exception {
		mockMvc.perform(get("/addItem"))
		.andExpect(status().isOk())
		.andExpect(view().name("forward:/bookDetail?id="))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}
	@Test
	public void testUpdateCartItemPage() throws Exception {
		mockMvc.perform(get("/updateCartItem"))
		.andExpect(status().isOk())
		.andExpect(view().name("forward:/shoppingCart/cart"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}
	@Test
	public void testremoveItemPage() throws Exception {
		mockMvc.perform(get("/removeItem"))
		.andExpect(status().isOk())
		.andExpect(view().name("forward:/shoppingCart/cart"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}
}

