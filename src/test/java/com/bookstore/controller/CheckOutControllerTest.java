package com.bookstore.controller;
import static org.hamcrest.CoreMatchers.containsString;
import static
org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static
org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static
org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
@RunWith(SpringRunner.class)
@WebMvcTest(CheckoutController.class)
public class CheckOutControllerTest {
	@Autowired
	private MockMvc mockMvc;

	@Test
	public void testcheckoutPage() throws Exception {
		mockMvc.perform(get("/checkout"))
		.andExpect(status().isOk())
		.andExpect(view().name("checkout"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}

	@Test
	public void testcheckouutPage() throws Exception {
		mockMvc.perform(post("/checkout"))
		.andExpect(status().isOk())
		.andExpect(view().name("orderSubmittedPage"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}
	@Test
	public void tesTsetShippingAddressPage() throws Exception {
		mockMvc.perform(post("/setShippingAddresst"))
		.andExpect(status().isOk())
		.andExpect(view().name("checkout"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}
	@Test
	public void testSetPaymentMethod() throws Exception {
		mockMvc.perform(post("/setPaymentMethod"))
		.andExpect(status().isOk())
		.andExpect(view().name("checkout"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}

}
