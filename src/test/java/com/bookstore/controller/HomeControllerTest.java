package com.bookstore.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static
org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static
org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static
org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(HomeController.class)

public class HomeControllerTest {
	@Autowired
	private MockMvc mockMvc;

	@Test
	public void testHomePage() throws Exception {
		mockMvc.perform(get("/"))
		.andExpect(status().isOk())
		.andExpect(view().name("index"))
		.andExpect(content().string(
		containsString("RUSHBOOKSTORE")));
			
	}
	@Test
	public void testFAQPage() throws Exception {
		mockMvc.perform(get("/faq"))
		.andExpect(status().isOk())
		.andExpect(view().name("faq"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}
	@Test
	public void testHourPage() throws Exception {
		mockMvc.perform(get("/hour"))
		.andExpect(status().isOk())
		.andExpect(view().name("hour"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}
	@Test
	public void testBookShelfPage() throws Exception {
		mockMvc.perform(get("/bookshelf"))
		.andExpect(status().isOk())
		.andExpect(view().name("bookshelf"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}
	@Test
	public void testLoginPage() throws Exception {
		mockMvc.perform(get("/login"))
		.andExpect(status().isOk())
		.andExpect(view().name("myAccount"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}
	@Test
	public void testBookDetailPage() throws Exception {
		mockMvc.perform(get("/bookDetail"))
		.andExpect(status().isOk())
		.andExpect(view().name("bookDetail"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}
	@Test
	public void testforgetPasswordPage() throws Exception {
		mockMvc.perform(get("/forgetPassword"))
		.andExpect(status().isOk())
		.andExpect(view().name("myAccount"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}
	@Test
	public void testMyProfilePage() throws Exception {
		mockMvc.perform(get("/myProfile"))
		.andExpect(status().isOk())
		.andExpect(view().name("myProfile"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}
	@Test
	public void testListOfCreditCardsPage() throws Exception {
		mockMvc.perform(get("/listOfCreditCards"))
		.andExpect(status().isOk())
		.andExpect(view().name("myProfile"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}
	@Test
	public void testlistOfShippingAddressesPage() throws Exception {
		mockMvc.perform(get("/listOfShippingAddresses"))
		.andExpect(status().isOk())
		.andExpect(view().name("myProfile"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}
	@Test
	public void testAddNewCreditCardPage() throws Exception {
		mockMvc.perform(get("/addNewCreditCard"))
		.andExpect(status().isOk())
		.andExpect(view().name("myProfile"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}
	@Test
	public void testAddNewCreditCardPostPage() throws Exception {
		mockMvc.perform(post("/addNewCreditCard"))
		.andExpect(status().isOk())
		.andExpect(view().name("myProfile"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}
	@Test
	public void testAddNewShippingAddressPage() throws Exception {
		mockMvc.perform(get("/addNewShippingAddress"))
		.andExpect(status().isOk())
		.andExpect(view().name("myProfile"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}
	@Test
	public void testAddNewShippingAddressPostPage() throws Exception {
		mockMvc.perform(post("/addNewShippingAddress"))
		.andExpect(status().isOk())
		.andExpect(view().name("myProfile"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}
	@Test
	public void testUpdateCreditCardPage() throws Exception {
		mockMvc.perform(get("/updateCreditCard"))
		.andExpect(status().isOk())
		.andExpect(view().name("myProfile"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}
	@Test
	public void testUpdateUserShippingPage() throws Exception {
		mockMvc.perform(get("/updateUserShipping"))
		.andExpect(status().isOk())
		.andExpect(view().name("myProfile"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}
	@Test
	public void setDefaultPaymentPage() throws Exception {
		mockMvc.perform(get("/setDefaultPaymentg"))
		.andExpect(status().isOk())
		.andExpect(view().name("myProfile"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}
	@Test
	public void setDefaultPaymentPagePost() throws Exception {
		mockMvc.perform(post("/setDefaultPaymentg"))
		.andExpect(status().isOk())
		.andExpect(view().name("myProfile"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}
	@Test
	public void setDefaultShippingAddress() throws Exception {
		mockMvc.perform(get("/setDefaultShippingAddress"))
		.andExpect(status().isOk())
		.andExpect(view().name("myProfile"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}
	@Test
	public void setDefaultShippingAddressPost() throws Exception {
		mockMvc.perform(post("/setDefaultShippingAddress"))
		.andExpect(status().isOk())
		.andExpect(view().name("myProfile"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}
	@Test
	public void removeCreditCard() throws Exception {
		mockMvc.perform(get("/removeCreditCard"))
		.andExpect(status().isOk())
		.andExpect(view().name("myProfile"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}
	@Test
	public void removeUserShipping() throws Exception {
		mockMvc.perform(get("/removeUserShipping"))
		.andExpect(status().isOk())
		.andExpect(view().name("myProfile"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}
	@Test
	public void newUser() throws Exception {
		mockMvc.perform(get("/newUse"))
		.andExpect(status().isOk())
		.andExpect(view().name("myProfile"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}
	@Test
	public void newUserPost() throws Exception {
		mockMvc.perform(post("/newUse"))
		.andExpect(status().isOk())
		.andExpect(view().name("myProfile"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}
	@Test
	public void updateUserInfo() throws Exception {
		mockMvc.perform(get("/updateUserInfo"))
		.andExpect(status().isOk())
		.andExpect(view().name("myProfile"))
		.andExpect(content().string(
		containsString("RUSH BOOKSTORE")));
			
	}
	
	
	
}
