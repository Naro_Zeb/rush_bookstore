*************************************************************
rush_bookstore Project 
*************************************************************
    Group members(IT)
        Nathneal Aberra ATR/6281/08
        Nahom Hailu ATR/6097/09
        Nardos Zebene ATR/8924/09
        Mubarek Ahmedin ATR/6501/09
        Yohannes Dawit ATR/2654/09


*************************************************************
Project Configuration
*************************************************************
    We use mysql DBMS for the project with no password as an input application properties and 
    the name of the database is bookstoredatabase.
    User and Password are stored in the database:
    example it loads:
                user: 'j'
                password: 'p'
                
                Another account:
                    user: 'rush'
                    password: '12345'
